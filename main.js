const autocaptureButton = document.querySelector('.autocaptureButton')
const livenessButton = document.querySelector('.livenessButton')
const fvdButton = document.querySelector('.fvdButton')
//const dimages = document.querySelector('.dimages')
const infotext = document.querySelector('.infotext')
const text = document.querySelector('.texto')
const s_id_display = document.querySelector('.s_id_display')
const auto_script = document.getElementById('auto_source')
const live_script = document.getElementById('live_source')
const amb_html = document.getElementById('amb')
let boxes = document.querySelector('.checkboxes')
let contact = document.querySelectorAll('input[name="test"]')
let testing = document.querySelector('.radio').checked
var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

let autocapture_token_front
let autocapture_token_back
let liveness_token
let img1
let img2
let img3
let ambient
let check_splunk = 0

if(amb_html.innerText == "dev")ambient = 0
else if(amb_html.innerText == "sandbox")ambient = 1
else if(amb_html.innerText == "prod")ambient = 2


contact[0].addEventListener("change", function() {
    ambient = 0
    location.href = "./layout_dev.html";
})

contact[1].addEventListener("change", function() {
    ambient = 1
    location.href = "./layout_sand.html";
})

contact[2].addEventListener("change", function() {
    ambient = 2
    location.href = "./layout_prod.html";
})

/*
dimages.addEventListener('click', (e)=>{

    boxes.classList.toggle('show-boxes')
});
*/

autocaptureButton.addEventListener('click', (e)=>{
    callAuto()
});

livenessButton.addEventListener('click', (e)=>{
    callLive()
});

fvdButton.addEventListener('click', (e)=>{
    callFvD()
});