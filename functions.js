const { info } = require("console");
const { url } = require("inspector");

function callAuto() {
    clear_window()
    var urlencoded = new URLSearchParams();
    urlencoded.append("autocapture", "true");
    urlencoded.append("ambient", ambient)

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    fetch("http://54.236.27.111/autocapture", requestOptions)
    .then(response => response.text())
    .then(result => {

        
        const ans = JSON.parse(result)
        const session = ans.session_id

        s_id_display.innerHTML = ("session id: " + session)

        let alt_s
        let url_l
        if(ambient == 0){
            alt_s = "https://dev-capture.tocws.com"
            url_l = "https://dev-api.7oc.cl/auto-capture/v1"
        }
        else if(ambient == 1){
            alt_s = "https://sandbox-capture.tocws.com"
            url_l = "https://sandbox-api.7oc.cl/auto-capture/data/v2"
        }
        else if(ambient == 2){
            alt_s = "https://prod-capture-ms.tocws.com"
            url_l = "https://prod-api.7oc.cl/auto-capture/data/v2"
        }

        TOCautocapture(ventana, {
            locale: "es",
            session_id: session,
            document_type: "CHL2",
            document_side: "front",
            http: true,
            callback: function(captured_token, image){
                getAutocaptureTokenFront(captured_token)
                document.getElementById('img1').src = image
                s_id_display.innerHTML += ("\ntoken: " + captured_token)
                check_splunk = 1
                setTimeout(callSplunk, 9000, captured_token)

            },
            failure: function(error){
                //alert(error);
                autocapture_error(error)
            },
            alt_server: alt_s,
            url_lbac: url_l
        });
        
    })
    .catch(error => console.log('error', error));
}

function callLive() {
    clear_window()
    var urlencoded = new URLSearchParams();
    urlencoded.append("liveness", "true");
    urlencoded.append("mode", "1")
    urlencoded.append("ambient", ambient)

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    fetch("http://54.236.27.111/liveness", requestOptions)
    .then(response => response.text())
    .then(result => {
        const ans = JSON.parse(result)
        const session = ans.session_id

        s_id_display.innerHTML = ("session id:" + session)

        let alt_s
        let url_l
        if(ambient == 0){
            alt_s = "https://dev-liveness.tocws.com"
            url_l = "https://dev-api.7oc.cl/liveness/image-saver/v1"
        }
        else if(ambient == 1){
            alt_s = "https://sandbox-liveness.tocws.com"
            url_l = "https://sandbox-api.7oc.cl/liveness/image-saver/v1"
        }
        else if(ambient == 2){
            alt_s = "https://prod-liveness.tocws.com"
            url_l = "https://prod-api.7oc.cl/liveness/image-saver/v1"
        }

        TOCliveness(ventana, {
            locale: "en",
            session_id: session,
            http: true,
            callback: function(token, image){
                getLivenessToken(token) 
                document.getElementById('img1').src = image
                s_id_display.innerHTML += ("\ntoken: " + token)
                check_splunk = 2
                setTimeout(callSplunk, 9000, token)
            },
            failure: function(error){
                liveness_error(error)
            },
            alt_server: alt_s,
            url_lbliv: url_l
        });
    })
    .catch(error => console.log('error', error));
}

function callFvD(){
    clear_window()
    var urlencoded = new URLSearchParams();
    urlencoded.append("autocapture", "true");
    urlencoded.append("liveness", "true")
    urlencoded.append("mode", "1")
    urlencoded.append("ambient", ambient)

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    fetch("http://54.236.27.111/fvd", requestOptions)
    .then(response => response.text())
    .then(result => {
        const ans = JSON.parse(result)
        const session = ans.session_id

        s_id_display.innerHTML = ("session id: " + session)

        let alt_s1
        let url_l1
        if(ambient == 0){
            alt_s1 = "https://dev-capture.tocws.com"
            url_l1 = "https://dev-api.7oc.cl/auto-capture/v1"
        }
        else if(ambient == 1){
            alt_s1 = "https://sandbox-capture.tocws.com"
            url_l1 = "https://sandbox-api.7oc.cl/auto-capture/data/v2"
        }
        else if(ambient == 2){
            alt_s1 = "https://prod-capture-ms.tocws.com"
            url_l1 = "https://prod-api.7oc.cl/auto-capture/data/v2"
        }

        let alt_s2
        let url_l2
        if(ambient == 0){
            alt_s2 = "https://dev-liveness.tocws.com"
            url_l2 = "https://dev-api.7oc.cl/liveness/image-saver/v1"
        }
        else if(ambient == 1){
            alt_s2 = "https://sandbox-liveness.tocws.com"
            url_l2 = "https://sandbox-api.7oc.cl/liveness/image-saver/v1"
        }
        else if(ambient == 2){
            alt_s2 = "https://prod-liveness.tocws.com"
            url_l2 = "https://prod-api.7oc.cl/liveness/image-saver/v1"
        }

        TOCautocapture(ventana, {
            locale: "es",
            session_id: session,
            document_type: "CHL2",
            document_side: "front",
            http: true,
            callback: function(captured_token, image){
                img1 = image
                getAutocaptureTokenFront(captured_token)
                
                TOCautocapture(ventana1, {
                    locale: "es",
                    session_id: session,
                    document_type: "CHL2",
                    document_side: "back",
                    http: true,
                    callback: function(captured_token2, image2){
                        img2 = image2
                        getAutocaptureTokenBack(captured_token2)
                        
                        TOCliveness(ventana, {
                            locale: "es",
                            session_id: session,
                            http: true,
                            callback: function(token, image3){ 
                                img3 = image3
                                getLivenessToken(token)
                                callApiFacial()
                                document.getElementById('img1').src = img1
                                document.getElementById('img2').src = img2
                                document.getElementById('img3').src = img3
                            },
                            failure: function(error){
                                liveness_error(error)    
                            },
                            alt_server: alt_s2,
                            url_lbliv: url_l2
                        });
                    },
                    failure: function(error){
                        autocapture_error(error)
                    },
                    alt_server: alt_s1,
                    url_lbac: url_l1
                });
            },
            failure: function(error){
                autocapture_error(error)
            },
            alt_server: alt_s1,
            url_lbac: url_l1
        });
    })
    .catch(error => console.log('error', error));
}

function callApiFacial(){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    var urlencoded = new URLSearchParams();
    urlencoded.append("id_front", autocapture_token_front);
    urlencoded.append("id_back", autocapture_token_back);
    urlencoded.append("selfie", liveness_token);
    urlencoded.append("ambient", ambient)

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    fetch("http://54.236.27.111/apifacial", requestOptions)
    .then(response => response.text())
    .then(result => {
        //printResult(result)
        console.log(result)

        var sp = result.split("\"")
        toc_token = sp[3]
        s_id_display.innerHTML += (" token: " + toc_token)


        check_splunk = 3
        setTimeout(callSplunk, 9000, toc_token)
    })
    .catch(error => console.log('error', error));
}

function callSplunk(session_id){
    var urlencoded = new URLSearchParams();
        urlencoded.append("session_id", session_id);
        urlencoded.append("ambient", ambient)

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        fetch("http://54.236.27.111/splunk", requestOptions)
        .then(response => response.text())
        .then(result => {
            console.log(result)
            const ans = JSON.parse(result)
            console.log(ans)

            var sp = ans.split(" ")

            if(check_splunk == 1){
                s_id_display.innerHTML += (" " + sp[3]) //token
                s_id_display.innerHTML += (" " + sp[14]) //session id
                //s_id_display.innerHTML += (" " + sp[17]) //status

                if(autocapture_token_front == sp[3].split("\"")[1])
                    s_id_display.innerHTML += (" Token coincide")
                if(sp[17].split("\"")[1] == '200')
                    s_id_display.innerHTML += (", proceso finalizado correctamente")
            } else if(check_splunk == 2){
                s_id_display.innerHTML += (" " + sp[14])  //token
                s_id_display.innerHTML += (" " + sp[19])  //session id
                //s_id_display.innerHTML += (" " + sp[21])  //status

                if(liveness_token == sp[14].split("\"")[1])
                    s_id_display.innerHTML += (" Token coincide")
                if(sp[21].split("\"")[1] == '200')
                    s_id_display.innerHTML += (", proceso finalizado correctamente")
            } else if(check_splunk == 3){
                s_id_display.innerHTML += (" " + sp[2]) //api version
                s_id_display.innerHTML += (" " + sp[3])  //back autocapture token
                s_id_display.innerHTML += (" " + sp[10])  //front autocapture token
                if(ambient == 0 || ambient == 2){
                    s_id_display.innerHTML += (" " + sp[38])  //liveness token
                    //s_id_display.innerHTML += (" " + sp[42])  //status

                    if(autocapture_token_back == sp[3].split("\"")[1] &&
                    autocapture_token_front == sp[10].split("\"")[1] &&
                    liveness_token == sp[38].split("\"")[1])
                    s_id_display.innerHTML += (" Todos los tokens coinciden")
                    if(sp[42].split("\"")[1] == '200')
                        s_id_display.innerHTML += (", proceso finalizado correctamente")
                } else{
                    s_id_display.innerHTML += (" " + sp[39])  //liveness token
                    //s_id_display.innerHTML += (" " + sp[43])  //status

                    if(autocapture_token_back == sp[3].split("\"")[1] &&
                    autocapture_token_front == sp[10].split("\"")[1] &&
                    liveness_token == sp[39].split("\"")[1])
                    s_id_display.innerHTML += (" Todos los tokens coinciden.")
                    if(sp[43].split("\"")[1] == '200')
                        s_id_display.innerHTML += (", proceso finalizado correctamente")
                }
            
            }
        })
        .catch(error => console.log('error', error));
}

function getAutocaptureTokenFront(captured_token){
    autocapture_token_front = captured_token
}

function getAutocaptureTokenBack(captured_token2){
    autocapture_token_back = captured_token2
}

function getLivenessToken(token){
    liveness_token = token
}

function clear_window(){
    document.getElementById('img1').src = ''
    document.getElementById('img2').src = ''
    document.getElementById('img3').src = ''
    s_id_display.innerHTML = ''
    infotext.innerHTML = ''
}

function autocapture_error(error){
    if(error == 401) s_id_display.innerHTML += ("\nError " + error + ": envio de datos fallido")
    else if(error == 402) s_id_display.innerHTML += ("\nError " + error + ": tiempo límite excedido")
    else if(error == 403) s_id_display.innerHTML += ("\nError " + error + ": cancelado por el usuario")
    else if(error == 404) s_id_display.innerHTML += ("\nError " + error + ": problemas de conexión a internet")
    else if(error == 405) s_id_display.innerHTML += ("\nError " + error + ": sesión expirada")
    else if(error == 406) s_id_display.innerHTML += ("\nError " + error + ": no hay conexión con el servidor")
    else if(error == 407) s_id_display.innerHTML += ("\nError " + error + ": cámara no disponible")
    else if(error == 408) s_id_display.innerHTML += ("\nError " + error + ": navegador no soportado")
    else if(error == 409) s_id_display.innerHTML += ("\nError " + error + ": no hay permiso de cámara")
    else if(error == 410) s_id_display.innerHTML += ("\nError " + error + ": cámara no cumple con resolución mínima")
    else if(error == 411) s_id_display.innerHTML += ("\nError " + error + ": falta session id")
    else if(error == 412) s_id_display.innerHTML += ("\nError " + error + ": falta document type")
    else if(error == 413) s_id_display.innerHTML += ("\nError " + error + ": falta document side")
    else if(error == 414) s_id_display.innerHTML += ("\nError " + error + ": falta iOS delegate")
    else if(error == 415) s_id_display.innerHTML += ("\nError " + error + ": falta iOS controller")
    else if(error == 421) s_id_display.innerHTML += ("\nError " + error + ": session id no válido")
    else if(error == 422) s_id_display.innerHTML += ("\nError " + error + ": document type no soportado")
    else if(error == 430) s_id_display.innerHTML += ("\nError " + error + ": cédula no pudo ser validada")
    else if(error == 431) s_id_display.innerHTML += ("\nError " + error + ": session id ya utilizado")
    else if(error == 450) s_id_display.innerHTML += ("\nError " + error + ": error de compatibilidad de ambientes")
    else if(error == 451) s_id_display.innerHTML += ("\nError " + error + ": problema de implementación")
    else if(error == 501) s_id_display.innerHTML += ("\nError " + error + ": error de cámara")
}

function liveness_error(error){
    if(error == 401) s_id_display.innerHTML += ("\nError " + error + ": envio de datos fallido")
    else if(error == 402) s_id_display.innerHTML += ("\nError " + error + ": se superó el tiempo del proceso")
    else if(error == 403) s_id_display.innerHTML += ("\nError " + error + ": cancelado por el usuario")
    else if(error == 404) s_id_display.innerHTML += ("\nError " + error + ": problemas de conexión a internet")
    else if(error == 405) s_id_display.innerHTML += ("\nError " + error + ": session id inválido")
    else if(error == 406) s_id_display.innerHTML += ("\nError " + error + ": endpoint sin resolver")
    else if(error == 407) s_id_display.innerHTML += ("\nError " + error + ": cámara no disponible")
    else if(error == 408) s_id_display.innerHTML += ("\nError " + error + ": navegador no soportado")
    else if(error == 409) s_id_display.innerHTML += ("\nError " + error + ": no hay permiso de cámara")
    else if(error == 410) s_id_display.innerHTML += ("\nError " + error + ": cámara no cumple con resolución mínima")
    else if(error == 411) s_id_display.innerHTML += ("\nError " + error + ": falta session id")
    else if(error == 430) s_id_display.innerHTML += ("\nError " + error + ": imagen de rostro no pasa validaciones")
    else if(error == 450) s_id_display.innerHTML += ("\nError " + error + ": error no controlado")
}